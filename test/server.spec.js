// Integration test

const expect = require("chai").expect;
const request = require("request");
const { response } = require("../src/server");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
  before("Start server before running tests", (done) => {
    server = app.listen(port, () => {
      console.log(`Server listening at: http://localhost:${port}`);
      done();
    });
  });

  describe("RGB to Hex conversion", () => {
    const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

    it("Returns status 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it("Returns the color in hex", (done) => {
      request(url, (error, response, body) => {
        expect(body).to.equal('ffffff');
        done();
      });
    });
  });

  describe("Hex to RGB conversion", () => {
    const url = `http://localhost:${port}/hex-to-rgb?hex=00ff00`;

    it("Returns status 200", (done) => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it("Returns color in rgb", (done) => {
      request(url, (error, response, body) => {
        expect(body).to.equal("0,255,0");
        done();
      });
    });
  });
  
  after("Stop server after testing", (done) => {
    server.close();
    done();
  });
})