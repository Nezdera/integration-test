// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
  describe("RGB to Hex conversion", () => {
    it("converts the basic colors", () => {
      const redHex = converter.rgbToHex(255,0,0); // ff0000 - red
      const greenHex = converter.rgbToHex(0,255,0); // 00ff00 - green
      const blueHex = converter.rgbToHex(0,0,255); // 0000ff - blue

      expect(redHex).to.equal("ff0000");
      expect(greenHex).to.equal("00ff00");
      expect(blueHex).to.equal("0000ff");
    });
  });
    
  describe("Hex to RGB conversion", () => {
    it("Converts Hex to RGB", () => {
      const hex1 = converter.hexToRgb("00ff00");
      const hex2 = converter.hexToRgb("ff0000");

      expect(hex1).to.equal("0,255,0");
      expect(hex2).to.equal("255,0,0");
    });
  });
});