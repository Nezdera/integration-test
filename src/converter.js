/**
 *  Padding outputs 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
  return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
  /**
   * Converts RGB to Hex string
   * @param {number} red 0-255
   * @param {number} green 0-255
   * @param {number} blue 0-255
   * @returns {string} hex value
   */
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16); // 0-255 -> 0-ff
    const greenHex = green.toString(16);
    const blueHex = blue.toString(16);

    return pad(redHex) + pad(greenHex) + pad(blueHex); // hex string with 6 characters
  },

  /**
   * @param {string} hex hex string of a color
   * @returns {string} rgb string
   */
  hexToRgb: (hex) =>  {
    var bigint = parseInt(hex, 16);
    var red = (bigint >> 16) & 255;
    var green = (bigint >> 8) & 255;
    var blue = bigint & 255;

    return red + "," + green + "," + blue;
  }
}